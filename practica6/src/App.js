import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import axios from "axios";

function App() {
  const [entradas, setEntradas] = useState(["hola"]);

  useEffect(() => {
    // Actualiza el título del documento usando la API del navegador
    axios.get("http://localhost:1337/practica-6-s").then((res) => {
      setEntradas(res.data);
    });
  });

  return (
    <div className="App">
      <h1>Consumiendo MongoDB</h1>
      {entradas.map((entrada, key) => {
        return <p key={key}>{entrada.entries}</p>;
      })}
    </div>
  );
}

export default App;
